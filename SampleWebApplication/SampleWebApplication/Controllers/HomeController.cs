﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerOrderServiceSample;


namespace SampleWebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page, with minor change.";
            Customer premiumCustomer = new Customer
            {
                CustomerId = 1,
                CustomerName = "Test",
                CustomerType = CustomerType.Premium
            };


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page, minor change by NG.....";

            return View();
        }
    }
}